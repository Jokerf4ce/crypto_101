


var app = {

    src : 'http://localhost:3000/test.json',
    range : 10

};


app.Generator = function (arr) {



    function Row(id){

        var row = document.createElement('div');
        row.setAttribute('id', id + '_row');
        row.setAttribute('class', 'row');

        return row
    }
    function Cell(id,classes,html){

        var cell = document.createElement('div');
        cell.setAttribute('id', id);
        cell.setAttribute('class', classes);
        cell.innerHTML = html;



        return cell;
    }

    for( var x=0; x< arr.length; x++ ) {

        var row = new Row(arr[x].symbol);


        var argv = [

            [ 'rank'                 , 'text col-xs-1 text-left left-pad',  arr[x].rank                 || 'ERR' ],
            [ 'symbol'               , 'text col-xs-1 text-center',         arr[x].symbol               || 'ERR' ],
            [ 'name'                 , 'text col-xs-2 text-left',           arr[x].name                 || 'ERR' ],
            [ 'price'                , 'text col-xs-2 text-left',           arr[x].price                || 'ERR' ],
            [ 'percent_change_1h'    , 'text col-xs-2 text-left',           arr[x].percent_change_1h    || 'ERR' ],
            [ 'percent_change_24h'   , 'text col-xs-2 text-left',           arr[x].percent_change_24h   || 'ERR' ],
            [ 'percent_change_7d'    , 'text col-xs-2 text-left',           arr[x].percent_change_7d    || 'ERR' ]


        ];

        for ( var y = 0; y < argv.length; y++ ) {

            var uid = arr[x].symbol;


            var cell = new Cell(  uid + '_' + argv[y][0],   uid + ' ' +argv[y][1],   argv[y][2]  );


            if (y===0) {



                var img = document.createElement('img');
                    img.setAttribute('class', uid + ' icon left-mad');
                    img.setAttribute('src', 'assets/coins/' + arr[x].id + '.png');
                    img.setAttribute('width', '32px');
                    img.setAttribute('height', '32px');

                cell.appendChild(img)
            }

            if ( cell.getAttribute('id') === uid +'_percent_change_1h' || cell.getAttribute('id') === uid +'_percent_change_24h'  || cell.getAttribute('id') === uid +'_percent_change_7d' ) {

                val = Number(cell.innerHTML);

                if (Math.sign(val) === -1) {

                    var i = document.createElement('i');
                    i.setAttribute('class', 'fa fa-angle-down');

                    cell.setAttribute('class', cell.getAttribute('class') + ' downturn');
                    cell.innerHTML = val + '   ';
                    cell.appendChild(i);

                    if (parseFloat(val) < parseFloat(-2.5)) {

                        var i = document.createElement('i');
                        i.setAttribute('class', 'fa fa-angle-double-down');

                        cell.setAttribute('class', cell.getAttribute('class') + ' xdownturn');
                        cell.innerHTML = val + '   ';
                        cell.appendChild(i);
                    }
                }


                if (val > 0) {

                    var i = document.createElement('i');
                    i.setAttribute('class', 'fa fa-angle-up');

                    cell.setAttribute('class', cell.getAttribute('class') + ' upturn');
                    cell.innerHTML = val + '&nbsp;&nbsp;&nbsp;';
                    cell.appendChild(i);

                    if (val > 2.5) {

                        var i = document.createElement('i');
                        i.setAttribute('class', 'fa fa-angle-double-up');

                        cell.setAttribute('class', cell.getAttribute('class') + ' xupturn');
                        cell.innerHTML = val + '&nbsp;&nbsp;&nbsp;';
                        cell.appendChild(i);
                    }
                }


            }


            row.appendChild(cell);


        }



        document.getElementById('main').appendChild(row);
    }

    document.getElementById('main').appendChild(row);


};

app.Coin = function (id,src) {


    var coin;

    for (var i = 0; i <  src.length; i++) {

        if (src[i].id === id) {

            coin = {};


            coin.id = src[i].id;
            coin.name = src[i].name;
            coin.symbol = src[i].symbol;
            coin.rank = src[i].rank;
            coin.price = src[i].price_chf;
            coin.market_cap_chf = src[i].market_cap_chf;
            coin.total_supply = src[i].total_supply;
            coin.available_supply = src[i].available_supply;
            coin.percent_change_1h = src[i].percent_change_1h;
            coin.percent_change_24h = src[i].percent_change_24h;
            coin.percent_change_7d = src[i].percent_change_7d;

        }
    }
    return coin;
};

app.Parse = function (arr) {

    var stack = [];

    arr = arr.slice(0,app.range);

    for (var i = 0; i< arr.length ; i++){

        stack.push( new app.Coin (arr[i].id, arr));


    }

    return stack;


};


app.Filter = function (key,arr,algorithm,negative) {

    var $element = $('#Filter_'+key+'_fx');
    var reverse = true;
    var overwrite = false;



    if ( overwrite === false && $element.hasClass('fa-angle-right') ) {



        console.log('fa-angle-right');
        if (!$element.hasClass('active')) { $element.addClass('active') }
        $element.addClass('fa-angle-up');
        $element.removeClass('fa-angle-right');

        overwrite = true;
        reverse = false;
    }


    if ( overwrite === false && $element.hasClass('fa-angle-up') ) {

        console.log('fa-angle-up');
        overwrite = true;
        reverse = false;
        if (!$element.hasClass('active')) { $element.addClass('active') }
        $element.addClass('fa-angle-down');
        $element.removeClass('fa-angle-up');

    }

   if ( overwrite === false && $element.hasClass('fa-angle-down') ) {

       console.log('fa-angle-down');
       overwrite = true;
       reverse = true;
       if (!$element.hasClass('active')) { $element.addClass('active') }
       $element.addClass('fa-angle-up');
       $element.removeClass('fa-angle-down');
   }








    for ( var i = 0; i < arr.length; i++) {


        var $this = $('#'+arr[i]);

        $this.removeClass('active');
        $this.removeClass('fa-angle-down');
        $this.removeClass('fa-angle-up');
        $this.addClass('fa-angle-right');

    }

    app.Render( { filter : { key : key, reverse : reverse , algorithm : algorithm , negative: negative } } );



};

function Request (method, url, hook) {


    var req = new XMLHttpRequest();
    req.onreadystatechange = function () {

        if (req.readyState === 4 && req.status === 200) {

            localStorage.setItem('Request', this.responseText);
            hook(localStorage.getItem('Request')) && localStorage.setItem('Request', null);
        }
    };
    // console.info(moment().format() + '\t ' + method + ' \t ' + url);
    req.open(method, url, true);
    req.send();


}

 function Logic_Sort(key, reverse, algorithm, negative ){


    var x = algorithm ?
        function(x) {return algorithm(x[key])} :
        function(x) {return x[key]};

    reverse = !reverse ? 1 : -1;



    if ( negative === true ) {

        return function (a, b) {

            a = Number(a);
            b = Number(b);
            return a = x(a), b = x(b), reverse * ((a > b) - (b > a));
        }


    } else {

        return function (a, b) {
            return a = x(a), b = x(b), reverse * ((a > b) - (b > a));
        }

    }

}


app.Render = function(argv) {

    Request('GET', app.src, function(res){




        var data = JSON.parse(res);
            data = app.Parse(data);


        if ( argv === undefined || argv === null ) {

            argv = {

                filter : {
                    key         : 'rank',
                    reverse     : false,
                    algorithm   : parseInt,
                    negative    : false
                }
            };

        }



        // console.log(' app.Render( { filter : { key : "'+argv.filter.key+'", reverse : '+argv.filter.reverse+' , algorithm : '+argv.filter.algorithm+'  } } ); ');


        data = data.sort(Logic_Sort(  argv.filter.key,  argv.filter.reverse,  argv.filter.algorithm, argv.filter.negative ) );


        document.getElementById('main').innerHTML = '';


        app.Generator(data);

    });



};







app.init = function () {

    app.Render();


};


app.init();