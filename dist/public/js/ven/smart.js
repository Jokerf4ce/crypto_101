

function find(id){
    return document.getElementById(id);
}


function getClasses(id){

    var element;      // DOM element
    var string;       // tmp string
    var stack;        // list of classes

    // catch
    try{ element = find(id) } catch (TypeError) { element=null }
    if( element!=null ) {

        string = element.getAttribute('class');
        stack = string.split(' ');
        return stack;
    }
}

function getClass(id,$){


    var arr = getClasses(id);
    var res = false;

    if ( arr && arr[0] ){
        for(var i=0;i<arr.length;i++){


            if(arr[i] === $) { res = true }

        }
    }

    return res;

}

function setClasses(id,arr){

    var element;    // DOM element
    var string;     // tmp string

    string = '';

    //catch
    try { element = find(id) } catch (TypeError) { element=null }
    if(element!=null) {

        for(var i=0;i<arr.length;i++){

            if (i>0)    { string += ' ' + arr[i]; }
            else        { string = arr[i];}
        }

        element.setAttribute('class',string);
    }
}

function addClass(id,val){

    var element;    // DOM Object
    var string;     // tmp string

    // failsave non-existent id
    try{ element = find(id) } catch (TypeError) { element=null }
    if(element!=null) {

        string = element.getAttribute('class');
        element.setAttribute('class',string +' '+ val);

    }
}

function removeClass(id,val){

    var item;   // DOM Object
    var x;      // tmp var

    try{item = find(id)} catch(TypeError){item=null}
    if(item!=null){
        x = getClasses(id);
        for(var i=0;i<x.length;i++){
            if(x[i]==val){
                x.splice(i,1);
            }
        }
        item.setAttribute('class',x);

    }
}

function banClass(val){

    var item;   // DOM Object
    var x;      // tmp var

    item = document.getElementsByClassName(val);

    for(var n=0;n<item.length;n++){

        var id = item[n].getAttribute('id');
        x = getClasses(id);
        for(var i=0;i<x.length;i++){
            if(x[i]==val){ x.splice(i,1);}
        }
        setClasses(id,x);
    }
}
