


function Theme(theme){

    arc.set('theme', theme || 'cyborg');


    if ( document.getElementById('css_theme') ) {
        document.getElementById('css_theme').setAttribute('href','/css/ven/bs/'+theme+'/bootstrap.css');
        document.getElementById('css_theme_min').setAttribute('href','/css/ven/bs/'+theme+'/bootstrap.min.css');
    }
    else {


        var link = document.createElement('link');
            link.setAttribute('id','css_theme');
            link.setAttribute('rel','stylesheet');
            link.setAttribute('href','/css/ven/bs/'+theme+'/bootstrap.css');
        document.head.appendChild(link);

        var link2 = document.createElement('link');
            link2.setAttribute('id','css_theme_min');
            link2.setAttribute('rel','stylesheet');
            link2.setAttribute('href','/css/ven/bs/'+theme+'/bootstrap.min.css');
        document.head.appendChild(link2);

    }

}

Theme( arc.get('theme') || 'cyborg');
