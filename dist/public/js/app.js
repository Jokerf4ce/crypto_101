

function Request (method, url, hook) {


    var req = new XMLHttpRequest();
    req.onreadystatechange = function () {

        if (req.readyState === 4 && req.status === 200) {

            localStorage.setItem('Request', this.responseText);
            hook(localStorage.getItem('Request')) && localStorage.setItem('Request', null);
        }
    };
    // console.info(moment().format() + '\t ' + method + ' \t ' + url);
    req.open(method, url, true);
    req.send();


}




function Config(req){


    req = req.slice(0,10);

    var stack = [];

    for (var i = 0; i< req.length ; i++){

        stack.push( new Coin (req[i].id, req));


    }

    return stack;


}

var sort_by = function(field, reverse, primer){

    var key = primer ?
        function(x) {return primer(x[field])} :
        function(x) {return x[field]};

    reverse = !reverse ? 1 : -1;

    return function (a, b) {
        return a = key(a), b = key(b), reverse * ((a > b) - (b > a));
    }
};




function Generator(arr){



    function Row(id){

        var row = document.createElement('div');
        row.setAttribute('id', id + '_row');
        row.setAttribute('class', 'row');

        return row
    }
    function Cell(id,classes,html){

        var cell = document.createElement('div');
        cell.setAttribute('id', id);
        cell.setAttribute('class', classes);
        cell.innerHTML = html;



        return cell;
    }

    for( var x=0; x< arr.length; x++ ) {

        var row = new Row(arr[x].symbol);


        var argv = [

            [ 'rank'                 , 'text col-xs-1 text-left left-pad',  arr[x].rank                 || 'ERR' ],
            [ 'symbol'               , 'text col-xs-1 text-center',         arr[x].symbol               || 'ERR' ],
            [ 'name'                 , 'text col-xs-2 text-left',           arr[x].name                 || 'ERR' ],
            [ 'price_chf'            , 'text col-xs-2 text-left',           arr[x].price_chf            || 'ERR' ],
            [ 'percent_change_1h'    , 'text col-xs-2 text-left',           arr[x].percent_change_1h    || 'ERR' ],
            [ 'percent_change_24h'   , 'text col-xs-2 text-left',           arr[x].percent_change_24h   || 'ERR' ],
            [ 'percent_change_7d'    , 'text col-xs-2 text-left',           arr[x].percent_change_7d    || 'ERR' ]


        ];

        for ( var y = 0; y < argv.length; y++ ) {

            var uid = arr[x].symbol;


            var cell = new Cell(  uid + '_' + argv[y][0],   uid + ' ' +argv[y][1],   argv[y][2]  );


            if (y===0) {



                var img = document.createElement('img');
                img.setAttribute('class', uid + ' icon left-mad');

                img.setAttribute('src', 'assets/coins/' + arr[x].id + '.png');
                img.setAttribute('width', '32px');
                img.setAttribute('height', '32px');

                cell.appendChild(img)
            }

            if ( cell.getAttribute('id') === uid +'_percent_change_1h' || cell.getAttribute('id') === uid +'_percent_change_24h'  || cell.getAttribute('id') === uid +'_percent_change_7d' ) {

                val = Number(cell.innerHTML);

                if (Math.sign(val) === -1) {

                    var i = document.createElement('i');
                    i.setAttribute('class', 'fa fa-angle-down');

                    cell.setAttribute('class', cell.getAttribute('class') + ' downturn');
                    cell.innerHTML = val + '   ';
                    cell.appendChild(i);

                    if (parseFloat(val) < parseFloat(-2.5)) {

                        var i = document.createElement('i');
                        i.setAttribute('class', 'fa fa-angle-double-down');

                        cell.setAttribute('class', cell.getAttribute('class') + ' xdownturn');
                        cell.innerHTML = val + '   ';
                        cell.appendChild(i);
                    }
                }


                if (val > 0) {

                    var i = document.createElement('i');
                    i.setAttribute('class', 'fa fa-angle-up');

                    cell.setAttribute('class', cell.getAttribute('class') + ' upturn');
                    cell.innerHTML = val + '&nbsp;&nbsp;&nbsp;';
                    cell.appendChild(i);

                    if (val > 2.5) {

                        var i = document.createElement('i');
                        i.setAttribute('class', 'fa fa-angle-double-up');

                        cell.setAttribute('class', cell.getAttribute('class') + ' xupturn');
                        cell.innerHTML = val + '&nbsp;&nbsp;&nbsp;';
                        cell.appendChild(i);
                    }
                }


            }


            row.appendChild(cell);


        }



        document.getElementById('main').appendChild(row);
    }

    document.getElementById('main').appendChild(row);


}


function Update(filter){



    console.log( 'Update({ entity: \''+filter.entity+'\',  field: \''+ filter.field +'\', reverse: '+ filter.reverse +', primer: '+filter.primer +'});');

    $('#main').innerHTML = '';



    Request('GET', 'http://localhost:3000/test.json', function(res){

        setTimeout(function () {


            res = JSON.parse(res);

            document.getElementById('API_updated').innerHTML = moment.unix(res[0].last_updated).format();
            document.getElementById('API_updated').setAttribute('data-timeout',  Date.now() - (res[0].last_updated*1000));


            var conf = Config(res);

            if ( filter ){ try { conf = conf.sort(sort_by(filter.field , filter.reverse, filter.primer)); } catch(err){} }

            document.getElementById('main').innerHTML = '';

            Generator(conf,function () {
                window.setInterval(function () {
                    nextUpdate();
                },10)
            });

        },10);

    });

}


function nextUpdate(){


    var date = document.getElementById('API_updated').getAttribute('data-timeout');
    if( date <= 0 ) { Update(); }

}
function init(){ Update({ entity: 'Filter' } ) }


function UpdateFilter(field,arr,primer){





    var element = document.getElementById('Filter_'+field+'_fx');

    if ( getClass(element,'fa-angle-up') || getClass(element,'fa-angle-right') ) {


        Update({ entity: 'Filter',  field: field, reverse: false , primer: primer });


        if ( !getClass(element,'active') ) { addClass(element,'active')}


        addClass(element,'fa-angle-down');
        removeClass(element,'fa-angle-down');
    }


    if ( getClass(element,'fa-angle-up') || getClass(element,'fa-angle-right') ) {


        Update({ entity: 'Filter',  field: field, reverse: false , primer: primer });


        if ( !getClass(element,'active') ) { addClass(element,'active')}


        addClass(element,'fa-angle-down');
        removeClass(element,'fa-angle-down');
    }


    for ( var i = 0; i < arr.length; i++) {


        var $this = $('#'+arr[i]);

        $this.removeClass('active');
        $this.removeClass('fa-angle-down');
        $this.removeClass('fa-angle-up');
        $this.addClass('fa-angle-right');

    }

}


$( document ).ready(function() {





    $('#Filter_rank').click(function (e) {


        new UpdateFilter (

            'rank',
            ['Filter_name_fx', 'Filter_price_fx', 'Filter_change_1h_fx', 'Filter_change_24h_fx', 'Filter_change_7d_fx'],
            parseInt

        )




    });


    $('#Filter_name').click(function (e) {


        new UpdateFilter (

            'name',
            ['Filter_rank_fx', 'Filter_price_fx', 'Filter_change_1h_fx', 'Filter_change_24h_fx', 'Filter_change_7d_fx'],
            function(a){return a.toUpperCase()}

        )

    });


});


init();
