
$( document ).ready(function() {




    $('#Filter_rank').click(function (e) {

        app.Filter (

            'rank',
            ['Filter_name_fx', 'Filter_price_fx', 'Filter_change_1h_fx', 'Filter_change_24h_fx', 'Filter_change_7d_fx'],
            parseInt

        )




    });
    $('#title_rank').click(function (e) {

        app.Filter (

            'rank',
            ['Filter_name_fx', 'Filter_price_fx', 'Filter_change_1h_fx', 'Filter_change_24h_fx', 'Filter_change_7d_fx'],
            parseInt

        )




    });


    $('#Filter_name_fx').click(function (e) {


        app.Filter (

            'name',
            ['Filter_rank_fx', 'Filter_price_fx', 'Filter_change_1h_fx', 'Filter_change_24h_fx', 'Filter_change_7d_fx'],
            (function(a){return a.toUpperCase()})

        )

    });
    $('#title_name').click(function (e) {


        app.Filter (

            'name',
            ['Filter_rank_fx', 'Filter_price_fx', 'Filter_change_1h_fx', 'Filter_change_24h_fx', 'Filter_change_7d_fx'],
            (function(a){return a.toUpperCase()})

        )

    });

    $('#Filter_price_fx').click(function (e) {


        app.Filter (

            'price',
            ['Filter_rank_fx', 'Filter_name_fx', 'Filter_change_1h_fx', 'Filter_change_24h_fx', 'Filter_change_7d_fx'],
            parseFloat

        )

    });

    $('#Filter_change_1h').click(function (e) {


        app.Filter (

            'change_1h',
            ['Filter_rank_fx', 'Filter_price_fx', 'Filter_price_fx', 'Filter_change_24h_fx', 'Filter_change_7d_fx'],
            parseFloat

        )

    });

    $('#Filter_change_24h_fx').click(function (e) {


        app.Filter (

            'change_24h',
            ['Filter_rank_fx', 'Filter_name_fx', 'Filter_price_fx', 'Filter_change_1h_fx', 'Filter_change_7d_fx'],
            parseFloat,
            true


        )

    });


    $('#Filter_change_7d_fx').click(function (e) {


        app.Filter (

            'change_7d',
            ['Filter_rank_fx', 'Filter_name_fx', 'Filter_price_fx', 'Filter_change_1h_fx', 'Filter_change_24h_fx'],
            parseFloat(Number())

        )

    });

});