
###     CLI Instructions
####    Git global setup

    git config --global user.name "Joker4ce"
    git config --global user.email "jokrfce@gmail.com"
    
####    Create a new repository        


    git clone https://Jokerf4ce@gitlab.com/Jokerf4ce/neon.git
    cd neon
    touch README.md
    git add README.md
    git commit -m "add README"
    git push -u origin master


####    Existing Project    

    cd existing_folder
    git init
    git remote add origin https://Jokerf4ce@gitlab.com/Jokerf4ce/neon.git
    git add .
    git commit
    git push -u origin master

####    Existing Git repository

    cd existing_repo
    git remote add origin https://Jokerf4ce@gitlab.com/Jokerf4ce/neon.git
    git push -u origin --all
    git push -u origin --tags

