#!/usr/bin/env node

// Jokerf4ce

// Node.js dependencies

{

var http = require('http');

}
//  ------------------ |

// npm dependencies
{

var debug = require('debug')('neon:server');
process.env['DEBUG'] = 'neon:server';

}
//  ------------------ |

// local dependencies
{

var neon = require('../neon.js');

}
//  ------------------ |

// global variables
{

var port = normalizePort(process.env.PORT || '3000');

}
neon.set('port', port);

var srv = http.createServer(neon);

srv.listen(port);
srv.on('error', err_hook);
srv.on('listening', listen_hook);

function normalizePort($) {
    var port = parseInt($, 10);
    if (isNaN(port)) { return $;}
    if (port >= 0) { return port; }
    return false;
}
function err_hook(err) {

    if (err.syscall !== 'listen') { throw err; }
    var bind = typeof port === 'string' ? 'Pipe ' + port : 'Port ' + port;

    switch (err.code) {

        case 'EACCES':
            console.error(bind + ' requires elevated privileges');
            process.exit(1);
            break;

        case 'EADDRINUSE':
            console.error(bind + ' is already in use');
            process.exit(1);
            break;

        default: throw err;

    }

}
function listen_hook() {

    var addr = srv.address();
    var bind = typeof addr === 'string' ? 'pipe ' + addr : 'port ' + addr.port;
    debug('Listening on ' + bind);

}



