
// Node.js dependencies
{

var path = require('path');

}

// npm dependencies
{

var body_parser     = require('body-parser');
var cookies         = require('cookie-parser');
var favicon         = require('serve-favicon');
var less            = require('less-middleware');
var morgan          = require('morgan');
var server          = require('express');
var should          = require('should');

}

// local dependencies

{

// config file

var config = require('./config.json');


// routes

var index   = require('./routes/index');


// misc

}

// global variables

{

var neon = server();

}

neon.set('views', './' + config.views);
neon.set('view engine', config.view_engine);



neon.use( favicon ( './' + config.favicon ) );
neon.use( morgan  ( 'dev' ) );
neon.use( body_parser.json() );
neon.use( body_parser.urlencoded( { extended: false } ) );
neon.use( cookies() );



neon.use( less( './' + config.static ) );
neon.use( server.static( './' + config.static ) );

neon.use( '/', index );

neon.use(function(req, res, next) {
    var err = new Error('Not Found');
    err.status = 404;
    next(err);
});

neon.use(function(err, req, res, next) {
    res.locals.message = err.message;
    res.locals.error = req.app.get('env') === 'development' ? err : {};
    res.status(err.status || 500);
    res.render('error');
});

module.exports = neon;
