
### Setup Instructions 

##  Dependencies

-   Node.js
-   pug  compiler ( if you don't use Node )
-   less compiler ( if you don't use Node )
-   Webserver     ( if you don't use Node )


##  Update Node Dependencies 

    npm install

##  Start the Server

    DEBUG=neon:* node ./srv/bin/daemon.js
    or  npm start